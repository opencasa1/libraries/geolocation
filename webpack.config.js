const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    main: path.resolve(__dirname, 'index.js')
  },
  output: {
    path: path.join(__dirname, 'public'),
    filename: '[name].dist.js'
  }
};
