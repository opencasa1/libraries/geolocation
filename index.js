const initAutocomplete = require('./src/autocomplete');

window.init = function init() {
  const element = document.querySelector('#autocomplete');

  initAutocomplete(element)
    .on('change', (res) => {
      const demoMap = document.querySelector('#staticmap');
      const latitude = res.coordinates.latitude;
      const longitude = res.coordinates.longitude;

      demoMap.src = `
        https://maps.googleapis.com/maps/api/staticmap?center=${latitude},${longitude}
          &zoom=17&size=400x400&markers=color:red%7C${latitude},${longitude}&key=AIzaSyB0A_F_DPrNkJfgBlAZDNMCVoj1hCJ0SE8&sid=` + Math.random();
    })
    .on('error', (err) => {
      console.error('error', err);
    })
};
