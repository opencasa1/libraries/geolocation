const StatusCodes = {
  OK: 'OK'
}

const makeService = () => {
  let geocoder = null;

  const callGeocode = (obj) => {
    return new Promise((resolve, reject) => {
      geocoder.geocode(obj, (results, status) => {
        if (status === StatusCodes.OK) {
          resolve(results);
        } else {
          reject(status);
        }
      });
    });
  };

  return {
    setGeocoder(geocoderInstance) {
      geocoder = geocoderInstance;
    },
    findAddress(address){
      return callGeocode({ address });
    },
    findLocation(location) {
      return callGeocode({ location });
    }
  };
};

module.exports = makeService();
