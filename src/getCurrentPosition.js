const getCurrentPosition = () =>
  new Promise((resolve, reject) => {
    const onSuccess = (position) => {
      resolve(position.coords);
    };
    const onError = (error) => {
      reject(error);
    };

    navigator.geolocation.getCurrentPosition(onSuccess, onError);
  });


module.exports = getCurrentPosition;
