class Observable {
  constructor() {
    this.events = {};
  }

  emit(eventKey, ...args) {
    this.events[eventKey].forEach(listener => {
      listener(...args);
    });
  }

  on(eventKey, listener) {
    if (!this.events[eventKey]) {
      this.events[eventKey] = [];
    }

    this.events[eventKey].push(listener);
    return this;
  }
}

module.exports = Observable;
