const geocodeService = require('./geocoder');
const Observable = require('./observable');

const initAutocomplete = (element) => {
  const observable = new Observable();
  const options = { types: ['geocode'] };
  const autocomplete = new google.maps.places.Autocomplete(element, options);
  const handler = onPlaceChanged({ autocomplete, element, observable });

  autocomplete.addListener('place_changed', handler);

  return observable;
};

const onPlaceChanged = ({ autocomplete, element, observable }) => () => {
  geocodeService.setGeocoder(new window.google.maps.Geocoder());

  const fixedAddress = normalizeAddress(element.value);

  geocodeService
    .findAddress(fixedAddress)
    .then(calibrateResults(fixedAddress))
    .then(results => {
      const [ selectedAddress ] = results;
      const { location } = selectedAddress.geometry;
      const place = autocomplete.getPlace();
      const formattedOutput = getAddressValues(place, fixedAddress);

      formattedOutput.coordinates = {
        latitude: location.lat(),
        longitude: location.lng()
      };
      observable.emit('change', formattedOutput);
    })
    .catch(reason => observable.emit('error', reason));
};

const normalizeAddress = (oldAddress) => {
  const newAddress = oldAddress.split(', ');
  const city = newAddress[1].toUpperCase();
  const country = newAddress[2].toUpperCase();

  if (city === 'SANTIAGO' && country === 'CHILE') {
    newAddress[1] = 'Santiago Centro';
  }

  return newAddress.join(', ');
};

const getAddressValues = (place, addressInput) => {
  const ac = place.address_components;
  const finder = field => ac.find(x => x.types[0] === field);
  const getNumber = text => ({ long_name: text.match(/\d+/)[0] });

  const values = {
    streetNumber: finder('street_number') || getNumber(addressInput),
    street: finder('route'),
    city: finder('locality') || finder('administrative_area_level_3'),
    county: finder('administrative_area_level_2'),
    state: finder('administrative_area_level_1'),
    country: finder('country'),
    zipcode: finder('postal_code')
  };

  const newValues = Object.keys(values)
    .map(key => ({ [key]: (key !== 'state') ? values[key].long_name : values[key].short_name }))
    .reduce((acc, next) => ({ ...acc, ...next }), {});


  return {
    ...newValues,
    get address(){
      return `${newValues.streetNumber} ${newValues.street} ${newValues.city} ${newValues.state} ${newValues.zipcode}`;
    }
  };
};

const calibrateResults = addressText => results => {
  if (results.length < 2) {
    return results;
  }

  const addressSegments = addressText.split(/,\s*/);

  const filteredResults = addressSegments.reduce((arr, part) =>
    arr.filter(r => r.formatted_address.includes(part)),
  results.slice());

  return filteredResults;
};


module.exports = initAutocomplete;
